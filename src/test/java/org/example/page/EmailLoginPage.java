package org.example.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.model.User;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;


public class EmailLoginPage extends AbstractPage{
    private final String urlUkrNetLoginPage = "https://accounts.ukr.net/login";
    private final Logger logger = LogManager.getRootLogger();

    public EmailLoginPage(WebDriver driver){
        super (driver);
    }

    @Override
    public EmailLoginPage openPage() {
        driver.get(urlUkrNetLoginPage);
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS))
                .until(ExpectedConditions.elementToBeClickable(loginField));
        logger.info(urlUkrNetLoginPage + "page is opened");
        return this;
    }

    @FindBy(xpath = "//input[@name = 'login']")
    private WebElement loginField;

    @FindBy(xpath = "//input[@name = 'password']")
    private WebElement passwordField;

    @FindBy(xpath = "/html/body/div/div/main/div[1]/form/p")
    private WebElement invalidPasswordMessage;

    @FindBy(xpath = "//button[@type = 'submit']")
    private WebElement continueButton;

    private void inputLogin(String login){
        loginField.clear();
        loginField.sendKeys(login);
    }

    private void inputPassword(String password){
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public boolean isPasswordInvalid(){
        return invalidPasswordMessage.isDisplayed();
    }

    public EmailUserPage logIn(User user){
        inputLogin(user.getUsername());
        inputPassword(user.getPassword());
        continueButton.click();
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS));
        logger.info("User with name" + user.getUsername() + "was logged in");
        return new EmailUserPage(driver);
    }

}
