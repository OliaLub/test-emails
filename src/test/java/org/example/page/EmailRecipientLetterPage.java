package org.example.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class EmailRecipientLetterPage extends AbstractPage{
    private static final Logger logger = LogManager.getLogger(EmailRecipientLetterPage.class);

    public EmailRecipientLetterPage(WebDriver driver){
        super(driver);
    }

    @Override
    protected AbstractPage openPage() {
        return null;
    }

    @FindBy(xpath = "//*[(@class = 'ii gt')]")
    public WebElement messageBody;

    public String readLetterBody(){
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[(@class = 'ii gt')]")));
        logger.info("the message was read");
        return messageBody.getText();
    }

}
