package org.example.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class EmailUserPage extends AbstractPage{
    private final Logger logger = LogManager.getRootLogger();

    public EmailUserPage(WebDriver driver){
        super (driver);
    }

    @Override
    public EmailUserPage openPage() {
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS));
        return this;
    }

    @FindBy(xpath = "//p[@class = 'login-button__user']")
    private WebElement emailAddressText;

    @FindBy(xpath = "//button[@class = 'login-button__control']")
    private WebElement accountSettingsMenu;

    @FindBy(id = "login__logout")
    private WebElement logOutButton;

    @FindBy(xpath = "//*[@id=\"content\"]/aside/button")
    private WebElement newLetterButton;

    @FindBy(xpath = "//input[@name ='toFieldInput']")
    private WebElement sendToTextBox;

    @FindBy(xpath = "//input[@name ='subject']")
    private WebElement subjectTextBox;

    @FindBy(id = "tinymce")
    private WebElement letterContentTextArea;

    @FindBy(xpath = "//*[@id=\"screens\"]/div/div[2]/div/button[1]")
    //@FindBy(xpath = "//*[@id= 'screens']//button[@class ='send']")
    private WebElement sendLetterButton;

    @FindBy(xpath = "//div[@class = 'sendmsg__ads-ready']")
    private WebElement messageWasSentText;

    public String getLoggedInEmailAddress(){
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS))
                .until(ExpectedConditions
                        .presenceOfElementLocated(By.xpath("/html/body/div[1]/div[2]/header/div[1]/div/div[2]/div[1]/p")));
        return emailAddressText.getText();
    }

    public EmailLoginPage logOut(){
        accountSettingsMenu.click();
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS))
                .until(ExpectedConditions.elementToBeClickable(logOutButton));
        logOutButton.click();
        logger.info("the user was logged out");
        return new EmailLoginPage(driver);
    }

    public EmailUserPage writeANewLetter(String recipientEmail, String letterSubject, String letterContent){
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS))
                .until(ExpectedConditions.elementToBeClickable(newLetterButton));
        newLetterButton.click();
        sendToTextBox.sendKeys(recipientEmail);
        subjectTextBox.sendKeys(letterSubject, Keys.TAB, letterContent);
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS))
                .until(ExpectedConditions.elementToBeClickable(sendLetterButton));
        sendLetterButton.click();
        logger.info("the email was sent to " + recipientEmail);
        return this;
    }

    public boolean messageWasSentSuccessfully(){
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS))
                .until(ExpectedConditions
                        .presenceOfElementLocated(By.xpath("//div[@class = 'sendmsg__ads-ready']")));
        return messageWasSentText != null;
    }
}
