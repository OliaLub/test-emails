package org.example.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.time.Duration;
import java.util.List;
import org.apache.logging.log4j.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class EmailRecipientUserPage extends AbstractPage{
    private static final Logger logger = LogManager.getLogger(EmailRecipientUserPage.class);

    public EmailRecipientUserPage(WebDriver driver){
        super(driver);
    }

    @Override
    protected AbstractPage openPage() {
        return null;
    }

   // @FindBy(xpath = "//tr[@class='zA zE']//div[@class='yW']//span[@class= 'zF']")
   // public List <WebElement> unreadLetters;

    @FindBy(xpath= "//*[@class='zA zE']//*[@class='yW']//*[@email='olha.hai.test@ukr.net']")
    public WebElement firstIncomeLetter;

    @FindBy(xpath = "//tbody/tr/td[4]/div[1]/span/span[@class='zF']")
    public WebElement firstIncomeLetterProperties;

   @FindBy(xpath =  "//a[@aria-label='Google apps']")
   private WebElement applicationsGoogleMenu;

   @FindBy(xpath = "//ul/li/a[contains(@href, 'https://mail.google.com/mail/?authuser=0')]")
   private WebElement gmailLink;

   public boolean isLetterUnread(){
       new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS))
               .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//tr[@class='zA zE']")));
       return firstIncomeLetter != null;
   }

   public String firstIncomeLetterSender(){
       new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS))
               .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//tr[@class='zA zE']")));
       return firstIncomeLetter.getAttribute("email");
   }

   public EmailRecipientLetterPage openFirstLetter(){
       new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS))
               .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//tr[@class='zA zE']")));

       firstIncomeLetter.click();
       logger.info("the letter was opened");
       return new EmailRecipientLetterPage(driver);
   }

    public void isPageNotGmail(){
        if (!driver.getCurrentUrl().contains("mail.google.com")){
            applicationsGoogleMenu.click();
            gmailLink.click();
        }
    }

}
