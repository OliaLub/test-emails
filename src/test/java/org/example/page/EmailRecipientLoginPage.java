package org.example.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.model.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class EmailRecipientLoginPage extends AbstractPage {
    private final String urlGmailLoginPage = "https://mail.google.com/";
    private static final Logger logger = LogManager.getLogger(EmailRecipientLoginPage.class);


    public EmailRecipientLoginPage(WebDriver driver){
        super(driver);
    }

    @Override
    public EmailRecipientLoginPage openPage() {
        driver.navigate().to(urlGmailLoginPage);
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS))
                .until(ExpectedConditions.elementToBeClickable(emailField));
        logger.info(urlGmailLoginPage + "page is opened");
        return this;
    }

    @FindBy(xpath = "//*[@id=\"view_container\"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div/div/ul/li[4]/div/div/div[2]")
    private WebElement chooseAnotherAccountLink;

    @FindBy(xpath = "//*[@id=\"identifierId\"]")
    private WebElement emailField;

    @FindBy(xpath = "//*[@id=\"identifierNext\"]/div/button/span")
    private WebElement continueLoginButton;

    @FindBy(xpath = "//*[@id=\"password\"]/div[1]/div/div[1]/input")
    private WebElement passwordField;

    @FindBy(xpath = "//*[@id=\"passwordNext\"]/div/button/span")
    private WebElement continuePasswordButton;

    private void inputEmail(String emailAddress){
        emailField.clear();
        emailField.sendKeys(emailAddress);
    }

    private void inputPassword(String password){
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public EmailRecipientUserPage logIn(User user){
   //     chooseAnotherAccountLink.click();
        inputEmail(user.getUsername());
        continueLoginButton.click();
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS))
                .until(ExpectedConditions.elementToBeClickable(passwordField));
        inputPassword(user.getPassword());
        continuePasswordButton.click();
        new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS));
        logger.info("User with name" + user.getUsername() + "was logged in");
        return new EmailRecipientUserPage(driver);
    }

}


