package org.example.service;

import org.example.model.User;

public class UserCreator {
    protected static final String USER_NAME = "testdata.user.name";
    protected static final String USER_PASSWORD = "testdata.user.password";
    public static final String USER_EMAIL = TestDataReader.getTestData(USER_NAME) + "@ukr.net";

    protected static final String USER_RECIPIENT_EMAIL = "testdata.recipientUser.name";
    protected static final String USER_RECIPIENT_PASSWORD = "testdata.recipientUser.password";

    public static User withCredentialsFromProperty(){
        return new User(TestDataReader.getTestData(USER_NAME), TestDataReader.getTestData(USER_PASSWORD));
    }

    public static User withEmptyUsername(){
        return new User("", TestDataReader.getTestData(USER_PASSWORD));
    }

    public static User withInvalidUsername(){
        return new User("abc", TestDataReader.getTestData(USER_PASSWORD));
    }

    public static User withEmptyPassword(){
        return new User(TestDataReader.getTestData(USER_NAME), "");
    }

    public static User withInvalidPassword(){
        return new User(TestDataReader.getTestData(USER_NAME), "abc");
    }

    public static User recipientWithCredentialsFromProperty(){
        return new User(TestDataReader.getTestData(USER_RECIPIENT_EMAIL), TestDataReader.getTestData(USER_RECIPIENT_PASSWORD));
    }

    public static User withInvalidCredentials(){
        return new User("abc", "abc");
    }

}
