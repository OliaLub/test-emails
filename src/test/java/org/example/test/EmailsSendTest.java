package org.example.test;

import org.example.model.User;
import org.example.page.EmailLoginPage;
import org.example.page.EmailRecipientLoginPage;
import org.example.page.EmailUserPage;
import org.example.service.UserCreator;
import org.testng.Assert;
import org.testng.annotations.Test;
import static org.example.service.UserCreator.USER_EMAIL;


public class EmailsSendTest extends CommonConditions{
    public static String letterMessage = "Hello!\nHave a nice day!";
    @Test
    public void sendLetterToOtherEmail(){
        User testUser = UserCreator.withCredentialsFromProperty();
        User testRecipient = UserCreator.recipientWithCredentialsFromProperty();
        EmailUserPage emailUserPage = new EmailLoginPage(driver)
                .openPage()
                .logIn(testUser)
                .writeANewLetter(testRecipient.getUsername(), "Hello letter", letterMessage);
        boolean isLetterWasSentSuccessfully = emailUserPage.messageWasSentSuccessfully();
        emailUserPage.logOut();
        Assert.assertTrue(isLetterWasSentSuccessfully,"The letter wasn't sent");
    }

    @Test (dependsOnMethods = {"sendLetterToOtherEmail"})
    public void verifyThatReceivedLetterIsUnread() {
        User testUser = UserCreator.recipientWithCredentialsFromProperty();
        boolean isLetterUnread = new EmailRecipientLoginPage(driver)
                .openPage()
                .logIn(testUser)
                .isLetterUnread();
        Assert.assertTrue(isLetterUnread, "The letter is read");
    }

    @Test (dependsOnMethods = {"sendLetterToOtherEmail"})
    public void verifyThatSenderIsAppropriate() {
        User testUser = UserCreator.recipientWithCredentialsFromProperty();

        String firstIncomeLetterSender = new EmailRecipientLoginPage(driver)
                .openPage()
                .logIn(testUser)
                .firstIncomeLetterSender();
        System.out.println("expected:   " + USER_EMAIL);
        System.out.println("actual  :   " + firstIncomeLetterSender);
        Assert.assertTrue(firstIncomeLetterSender.equals(USER_EMAIL), "Sender is not appropriate!");
    }

    @Test(dependsOnMethods = {"verifyThatReceivedLetterIsUnread", "verifyThatSenderIsAppropriate"})
    public void verifyThatReceivedLetterMessageEqualsSentMessage() {
        User testUser = UserCreator.recipientWithCredentialsFromProperty();
        String receivedLetterMessage = new EmailRecipientLoginPage(driver)
                .openPage()
                .logIn(testUser)
                .openFirstLetter()
                .readLetterBody();
        System.out.println("expected:   " + letterMessage);
        System.out.println("actual  :   " + receivedLetterMessage);
        Assert.assertEquals(receivedLetterMessage, letterMessage, "Received letter body doesn't match the sent letter body");
    }

}
