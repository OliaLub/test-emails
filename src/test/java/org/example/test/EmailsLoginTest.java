package org.example.test;

import org.example.model.User;
import org.example.page.EmailLoginPage;
import org.example.page.EmailUserPage;
import org.example.service.UserCreator;
import org.testng.annotations.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class EmailsLoginTest extends CommonConditions {

    @Test
    public void enterCorrectLoginAndPassword(){
      User testUser = UserCreator.withCredentialsFromProperty();
      EmailUserPage emailUserPage = new EmailLoginPage(driver)
              .openPage()
              .logIn(testUser);
      String loggedInUserEmailAddress = emailUserPage.getLoggedInEmailAddress();
      emailUserPage.logOut();
      assertThat(loggedInUserEmailAddress,is(equalTo(testUser.getUsername() + "@ukr.net")));
    }

    @Test
    public void enterCorrectLoginAndInvalidPassword(){
        User testUser = UserCreator.withInvalidPassword();
        new EmailLoginPage(driver)
                .openPage()
                .logIn(testUser);
        assertThat(driver.getCurrentUrl(),not(containsString("https://mail.ukr.net")));
    }

    @Test
    public void enterInvalidLoginAndCorrectPassword(){
        User testUser = UserCreator.withInvalidUsername();
        new EmailLoginPage(driver)
                .openPage()
                .logIn(testUser);
        assertThat(driver.getCurrentUrl(),not(containsString("https://mail.ukr.net")));
    }

    @Test
    public void enterInvalidLoginAndPassword(){
        User testUser = UserCreator.withInvalidCredentials();
        new EmailLoginPage(driver)
                .openPage()
                .logIn(testUser);
        assertThat(driver.getCurrentUrl(),not(containsString("https://mail.ukr.net")));
    }

    @Test
    public void enterCorrectLoginAndEmptyPassword(){
        User testUser = UserCreator.withEmptyPassword();
        new EmailLoginPage(driver)
                .openPage()
                .logIn(testUser);
        assertThat(driver.getCurrentUrl(),not(containsString("https://mail.ukr.net")));
    }

    @Test
    public void enterEmptyLoginAndCorrectPassword(){
        User testUser = UserCreator.withEmptyUsername();
        new EmailLoginPage(driver)
                .openPage()
                .logIn(testUser);
        assertThat(driver.getCurrentUrl(),not(containsString("https://mail.ukr.net")));
    }

}
