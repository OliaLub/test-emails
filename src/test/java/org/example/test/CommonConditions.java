package org.example.test;

import org.example.driver.DriverSingleton;
import org.example.util.TestListener;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

@Listeners({TestListener.class})
public class CommonConditions {
    protected WebDriver driver;

    @BeforeMethod(alwaysRun = true)
    public void setUpDriver() {
        driver = DriverSingleton.getDriver();
}

    @AfterMethod(alwaysRun = true)
    public void closeBrowser(){
        DriverSingleton.closeDriver();
}
}


